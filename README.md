# ros_event_logger

A command-line script for publishing [StringStamped]() messages to the topic `/notes`.   This allows recording of free-form, time-stamped notes into bagfiles.

## Usage

Consider setting:

```
alias event="rosrun event_logger event_logger"
```

Any text provided on the command line will be logged:

```
> event Started mission to mars today!
```

If no text is provided, the program will enter an interactive mode:

```
> event
Press enter to send. ctrl-d to quit >  I love going to mars!
```

Every line is published as a separate message.  ctrl-d exits the program.

## Data format

By default, each message is JSON-encoded:

```
> event Launch the rocket!
```

publishes:

```
header:
  seq: 1
  stamp:
    secs: 1706421167
    nsecs: 708428382
  frame_id: ''
data: "{\"content\": \"Launch the rocket!\", \"datetime\": \"2024-01-28T05:52:47.708852+00:00\"\
  , \"hostname\": \"bluerov2\", \"user\": \"amarburg\"}"
```

The JSON struct is a single map containing the following fields:

| field | description |
|-------|-------------|
| content | The message |
| datetime | The local system date/time with timezone offset |
| user | Username who posted message |
| hostname | Short hostname of system |

The `--plain` option causes the message to be published verbatim:

```
> event --plain Land the rocket!


header:
  seq: 1
  stamp:
    secs: 1706421215
    nsecs: 656425714
  frame_id: ''
data: "Land the rocket!"
```



## License

Released under the BSD 3-Clause license
