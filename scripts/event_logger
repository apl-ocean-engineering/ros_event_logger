#! /usr/bin/env python3
"""
Simple CLI that publishes notes typed by a human into the rosbag file.
This is slightly better than a `rostopic pub /notes std_msgs/String ...`
"""

import rospy
import apl_msgs.msg

import argparse
import json
from datetime import datetime
from socket import gethostname
from getpass import getuser
from tzlocal import get_localzone
import sys
import typing

# This "improves" input()
import readline

class EventLogger:
    def __init__(self, user : str = getuser(), plaintext : bool = False):
        """
        If plaintext=False (the default), messages will be wrapped in a JSON dict
        which includes local time, username, and computer hostname.

        If plaintext=True, messages will be published verbatim in the msg.data field.
        """

        self.plaintext = plaintext
        self.user = user
        self.pub = rospy.Publisher("/notes", apl_msgs.msg.StringStamped, queue_size=1)

    def post(self, message : str ) -> None:
        rosmsg = apl_msgs.msg.StringStamped()
        rosmsg.header.stamp = rospy.Time.now()

        if self.plaintext:
            rosmsg.data = message
        else:
            output = { "content": message }

            # n.b. this uses local time (time on the publishing system),
            # but includes the TZ offset
            output["datetime"] = datetime.now(tz=get_localzone()).isoformat()
            output["hostname"] = gethostname()
            output["user"] = self.user

            rosmsg.data = json.dumps(output)

        self.pub.publish(rosmsg)

    def interactive_mode(self) -> None:
        while not rospy.is_shutdown():
            try:
                user_input = input("Press enter to send. ctrl-d to quit > ")
                self.post(user_input)
            except EOFError:
                return


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
                prog='event_logger',
                description='Log event messages to /notes')

    parser.add_argument("--plain", action="store_true", default=False,
                        help="Log message as plain text, not JSON")

    parser.add_argument("--user", default=getuser(),
                        help="Username to associate with messages (defaults to current user)")

    parser.add_argument( "message", nargs="*", help="Message to post.  If not provided, script enters an interactive mode")
    args = parser.parse_args()

    rospy.init_node("event_logger", anonymous=True)
    el = EventLogger( user=args.user, plaintext=args.plain )

    if len(args.message) > 0:
        # Must remember that it takes a second or two for connections to get established
        # between setting up a publisher and actually publishing
        # (assume this isn't an issue in interactive mode)

        # 1 sec is an arbitrary/magic value.  Any way to avoid it?
        rospy.rostime.wallsleep(1.0)
        el.post(' '.join(args.message))
    else:
        el.interactive_mode()
